const axios = require('axios')

// typically an App Password created from a user's account settings
const username = process.env.PIPELINES_USERNAME
const password = process.env.PIPELINES_PASSWORD

if (!username) {
  console.log('No "PIPELINES_USERNAME" env var set')
  process.exit(1)
}
if (!password) {
  console.log('No "PIPELINES_PASSWORD" env var set')
  process.exit(1)
}

function triggerPipelines(configList) {
  const promises = []

  configList.forEach(({ branch, repo_slug, workspace }) => {
    if (!branch || !repo_slug || !workspace)
      throw new Error(
        `Downstream config missing params branch: ${branch}, repo_slug: ${repo_slug}, workspace: ${workspace}`,
      )

    const url = `https://api.bitbucket.org/2.0/repositories/${workspace}/${repo_slug}/pipelines/`
    console.log('url', url)

    const payload = {
      target: {
        ref_type: 'branch',
        type: 'pipeline_ref_target',
        ref_name: branch,
      },
    }

    promises.push(axios.post(url, payload, { auth: { username, password } }))
  })

  return Promise.all(promises).catch(e => {
    console.log(`POST ${e.request.socket._host}${e.request.path}`)
    console.log(e.response.data)
    console.log(e.response.status)
    console.log(e.response.statusText)

    throw new Error(
      'Not all downstream triggers successful (todo: make a slack notification)',
    )
  })
}

module.exports = triggerPipelines
