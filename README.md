# Bitbucket Utilities

A set of cli and node.js utility helpers for working with Bitbucket repositories

## Trigger Bitbucket Pipeline

Trigger a repo's Pipelines to run for a specific branch. See `example-pipeline-trigger.json` for formatting. This can be called either by importing in the node.js runtime or from the command line.

> PRO-TIP: Great to use for triggering downstream builds

Environment variables need to be set for the following:

`PIPELINES_USERNAME`

`PIPELINES_PASSWORD`

> The quick & easiest way to do this is by having a user who has access to go into their Bitbucket settings and create an "App Password" with specific access to "read/write pipeplines". Then that user can set those as Pipelines Variables under their config or the specific projects Pipelines env vars.

```
const triggerPipelines = require('pipeline-trigger')

const configs = [
  {
    "branch": "master",
    "workspace": "teamOrUser",
    "repo_slug": "repoName"
  }
]

triggerPipelines(configs)
```

### CLI

`npm-update --regex somenamepattern` This will match any dependency names found in `package.json` and run `npm update` for those packages

`trigger-pipelines [relative file path]` The file path is a JSON file that matches the file `example-pipeline-trigger.json` in this repo.
