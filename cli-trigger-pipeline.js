#!/usr/bin/env node
const fs = require('fs')
const path = require('path')

const triggerPipelines = require('./trigger-pipelines')

const [,, ...args] = process.argv

if (!args[0]) return console.log(`a JSON file path is required`)

const filePath = path.join(process.cwd(), args[0])
const file = fs.readFileSync(filePath)

if (!file) return console.log(`No file found at path: ${filePath}`)

let fileContents = file

if (!Array.isArray(fileContents))
  try {
    fileContents = JSON.parse(file)
  } catch (e) {
    return console.log('failed parsing JSON in file', e)
  }

if (!Array.isArray(fileContents)) return console.log(`File must contain an array format, see "example-pipeline-trigger.json"`)

console.log(`Triggering ${fileContents.length} pipelines`)

triggerPipelines(fileContents)
