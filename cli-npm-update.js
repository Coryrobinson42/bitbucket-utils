#!/usr/bin/env node
const fs = require('fs')
const path = require('path')
const args = require('args')
const exec = require('child_process').exec

const pkg = require(path.join(process.cwd(), 'package.json'))

args
  .option('regex', 'Match package names by regex')

const flags = args.parse(process.argv)

if (!flags.regex) return process.exit(0)

let pkgsToUpdate = []

const pkgNameRegEx = new RegExp(flags.regex)

const pkgList = [
  ...Object.keys(pkg.dependencies || {}),
  ...Object.keys(pkg.devDependencies || {})
]

pkgList.map(pkgName => {
  if (pkgNameRegEx.test(pkgName))
    pkgsToUpdate.push(pkgName)
})

if (pkgsToUpdate.length) {
  console.log(`Running: "npm update ${pkgsToUpdate.join(' ')}"`, )
  exec(`npm update ${pkgsToUpdate.join(' ')}`, (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`)
      return process.exit(1)
    }

    if (stdout) console.log(`stdout: ${stdout}`)
    if (stderr) console.log(`stderr: ${stderr}`)

    process.exit(0)
})
}
